"use strict";

class DelayDriver {
    constructor(stream, initialDelay) {
        this.context  = new AudioContext();

        this.source   = this.context.createMediaStreamSource(stream);
        this.analyser = this.context.createAnalyser();
        this.delay    = this.context.createDelay(60);

        this.source  .connect(this.analyser);
        this.analyser.connect(this.delay);
        this.delay   .connect(this.context.destination);
        
        this.delay.delayTime.value = initialDelay;
        this.analyser.fftSize = 1024;
        this.analysisBuffer = new Float32Array(this.analyser.fftSize);
    }

    static async create(initialDelay) {
        const audioStream = await navigator
            .mediaDevices
            .getUserMedia({ audio: true });
        
        return new DelayDriver(audioStream, initialDelay);
    }

    setDelay(delay) {
        if (Number.isNaN(delay)) { throw "Tried to set delay of NaN"; }
        this.delay.delayTime.value = delay;
    }

    getDelay() { return this.delay.delayTime.value; }

    maxDb() {
        this.analyser.getFloatTimeDomainData(this.analysisBuffer);
        return Math.max(...this.analysisBuffer.map(Math.abs));
    }
}

class Visualiser {
    constructor() {
        this.canvas = document.getElementById("visualiser");
        this.context = this.canvas.getContext("2d");
        this.previousFrameTime = Date.now();
        this.shiftRemainderPx = 0;

        this.resizeToFit();
    }

    resizeToFit() {
        const computedStyle = window.getComputedStyle(this.canvas);
        const canvasObject = this.context.canvas;

        canvasObject.width = parseFloat(computedStyle.width);
        canvasObject.height = parseFloat(computedStyle.height);
    }

    render(widthInSec, amplitude) {
        const widthInMsec = widthInSec * 1000;

        const now = Date.now();
        const frameMsec = now - this.previousFrameTime;

        this.previousFrameTime = now;

        const pixelsPerMsec = this.context.canvas.width / widthInMsec;
        const pixelShift = (frameMsec * pixelsPerMsec) + this.shiftRemainderPx;

        const pixelShiftWhole = Math.trunc(pixelShift);
        const pixelShiftFrac = pixelShift - pixelShiftWhole;
        const pixelShiftWholeBounded = Math.min(pixelShiftWhole, this.context.canvas.width);

        this.shiftCanvas(pixelShiftWholeBounded);
        this.drawAmplitude(pixelShiftWholeBounded, amplitude);

        this.shiftRemainderPx = pixelShiftFrac;
    }

    shiftCanvas(px) {
        const width = this.context.canvas.width - px;
        if (width == 0) { return; }

        const image = this.context.getImageData(
            0,
            0,
            width,
            this.context.canvas.height
        );

        this.context.putImageData(image, px, 0);
        this.context.clearRect(0, 0, px, this.context.canvas.height);
    }

    drawAmplitude(width, amplitude) {
        this.context.fillStyle = "#FFF";

        const height = amplitude * this.context.canvas.height * 0.75;
        const startY = (this.context.canvas.height - height) / 2;

        this.context.fillRect(0, startY, width, height);
    }

}

class Parakeet {
    constructor(delayDriver, visualiser) {
        this.delayDriver = delayDriver;
        this.visualiser = visualiser;
    }

    static async create(initialDelay) {
        const delayDriver = await DelayDriver.create(initialDelay);
        const visualiser = new Visualiser();

        return new Parakeet(delayDriver, visualiser);
    }

    render() {
        const delay = this.delayDriver.getDelay();
        const amplitude = this.delayDriver.maxDb();
        this.visualiser.render(delay, amplitude);
    }
}

var parakeet;

function getDelayInput() {
    const inputValue = document.getElementById("delay-seconds").value;
    return parseFloat(inputValue);
}

function updateParakeetSettings() {
    const delay = getDelayInput();
    if (Number.isNaN(delay)) { return; }

    parakeet.delayDriver.setDelay(delay);
}

function renderLoop() {
    parakeet.render();
    window.requestAnimationFrame(renderLoop);
}

async function initialize() {
    parakeet = await Parakeet.create(getDelayInput());
    
    Array
        .from(document.getElementById("controls").children)
        .filter(child => child.nodeName == "INPUT")
        .forEach(input => input.addEventListener(
            "input",
            updateParakeetSettings
        ));

    window.addEventListener(
        "resize",
        () => parakeet.visualiser.resizeToFit()
    );

    window.requestAnimationFrame(renderLoop);
}

document.addEventListener("DOMContentLoaded", initialize);
